import React from 'react';

const TaskItem = (props) => {
    const editTask = props.editTask;
    const taskKey = props.taskKey;
    const toggleEdit = props.toggleEdit;
    const updateList = props.updateList;
    //console.log(editTask) //Todo
    /* Add below to component
    <input type="text" value={props.taskList} className="edtTask" disabled={ !editTask } 
            onKeyPress={ (e) => updateList(e) }/> 
    <span className="glyphicon glyphicon glyphicon-pencil pull-right" title="edit" 
    onClick={ (e) => toggleEdit(e) } id={props.keyId}/>
    */
    return (
        <li className="listForStyle" id={props.keyId}>
            <span>{ props.taskList }</span>
            <span className="glyphicon glyphicon-remove-sign pull-right" title="remove"
            onClick={ () => props.removeTask(props.keyId) }
            />
        </li>
    )
}

export default TaskItem;