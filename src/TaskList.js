import React from 'react';
import TaskItem from './TaskItem';

const TaskList = (props) => {
    const list = props.list;
    const removeTask = props.removeTask;
    const editTask = props.editTask;
    const toggleEdit = props.toggleEdit;
    const taskKey = props.taskKey;
    const updateList = props.updateList;
    const tasks = list.map((task) => {
        return(
            <TaskItem
            key={task.key}
            taskList={task.task}
            keyId={task.key}
            removeTask={removeTask}
            editTask={editTask}
            toggleEdit={toggleEdit}
            list={list}
            taskKey={taskKey}
            updateList={updateList}
            />
        )
    })

    return(
        <ul>
            {tasks}
        </ul>
    )
}

export default TaskList;