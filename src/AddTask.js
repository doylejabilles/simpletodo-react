import React from 'react';

const AddTask = (props) => {
    const listEnter = props.addedTask;
    return(
        <div className="form-group">
            <label>Todo App:</label>
            <input type="text" className="form-control" 
            value={listEnter} 
            onKeyPress={props.addEventHandler}/>
            
            <input type="submit" value="Add"
            className="btn btn-default" 
            onClick={ props.addList }
            />
        </div>
    )
}

export default AddTask;