import React, {Component} from 'react';
import './index.css';
import AddTask from './AddTask';
import TaskList from './TaskList';

class App extends Component {
    constructor(props){
        super(props)

        this.state = {
            list: [],
            addedTask: '',
            editTask: false,
            taskKey: ''
        }

    }

    addEventHandler = (listValue) => {
        const keyCode = listValue.key;
        const value = listValue.target.value;
        return ( keyCode === 'Enter' ) ? this.addList(value) : this.setState({ addedTask: value })
    };
    
    /* TODO */
    toggleEdit = (taskKey) => {
        const key = taskKey.target.id;
        
        if( key !== this.state.taskKey) {
            this.setState({
                editTask: !this.state.editTask,
                taskKey: key
            })
        }
        
        console.log( this.state.editTask)
    }

    addList = (listAdded) => {
        const addedTask = listAdded;
        const key = Math.floor(100000 + Math.random() * 900000);
        if( addedTask.trim() ) {
            const listItem = {
                task: addedTask.trim(),
                key: key
            }
            
            this.setState({
                list: [...this.state.list, listItem]
            });
        }
    };

    /* TODO */
    updateList = (listUpdated) => {
        console.log(listUpdated);
    };

    removeTask = (task) => {
        const curTask = [...this.state.list];
        const removeTask = curTask.filter(e => e.key === task );
        curTask.splice(removeTask,1);
        this.setState({
            list: [...curTask]
        });
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="col-md-6 col-md-offset-3">
                    <div className="text-center">
                    <AddTask 
                    addEventHandler={ (listValue) => this.addEventHandler(listValue) } 
                    addList = {() => this.addList(this.state.addedTask)}
                    />
                    </div>
                    <div className="text-left">
                    <TaskList
                    list = {this.state.list}
                    removeTask = { (task) => this.removeTask(task) }
                    updateList = { (updatedTask) => this.updateList(updatedTask) }
                    editTask = { this.state.editTask }
                    task = {this.state.list}
                    toggleEdit = {(taskKey) => this.toggleEdit(taskKey)}
                    taskKey = {this.state.taskKey}
                    />
                    </div>
                </div>
            </div>
        );
    }
}

export default App;